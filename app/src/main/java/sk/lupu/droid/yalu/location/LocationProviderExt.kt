package sk.lupu.droid.yalu.location

import android.annotation.SuppressLint
import android.os.Looper
import android.util.Log
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.*
import sk.lupu.droid.yalu.data.datastore.UserSettingsModel
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.model.RouteData
import sk.lupu.droid.yalu.location.model.countIn
import sk.lupu.droid.yalu.location.model.toLocationData

private const val TAG = "LocationProviderExt"

@SuppressLint("MissingPermission")
fun FusedLocationProviderClient.getUserLocation(_currentLocationStateFlow: MutableStateFlow<LocationData>) {
    lastLocation.addOnSuccessListener { location ->
        Log.d(TAG, "current location: $location")
        _currentLocationStateFlow.update { location.toLocationData() }
    }
}

/**
 * .locationFlow()
 * Implementation of a cold flow backed by a Channel that sends Location updates
 *
 * Initialize FusedLocationProviderClient
 * Then use in in viewModelScope coroutine like so :
 * @sample:
 *
 *
 */
@SuppressLint("MissingPermission")
fun FusedLocationProviderClient.locationFlow(
    settings: UserSettingsModel,
) = callbackFlow {
    val callback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            try {
                trySendBlocking(result).isSuccess
            } catch (e: Exception) {
                Log.e(TAG, "onLocationResult: ", e)
            }
        }
    }
    // Start location updates
    requestLocationUpdates(
        createLocationRequest(settings),
        callback,
        Looper.getMainLooper()
    ).addOnFailureListener { e ->
        Log.e(TAG, "locationFlow: ", e)
        close(e) // in case of exception, close the Flow
    }

    // clean up when Flow collection ends
    awaitClose {
        // stop updates after flow is closed
        removeLocationUpdates(callback)
        Log.e(TAG, "locationFlow > awaitClose: REMOVED LOC UPDATES")
    }
}.distinctUntilChanged()

fun createLocationRequest(settings: UserSettingsModel): LocationRequest {
    return LocationRequest.create().apply {
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        interval = settings.trackingInterval

    }
}

fun LocationResult.handleLocationResults(
    _currentLocationStateFlow: MutableStateFlow<LocationData>,
    _currentRouteStateFlow: MutableStateFlow<RouteData>,
    _locationsStateFlow: MutableStateFlow<List<LocationData>>,
) {
    val location = this.lastLocation

    val routeData = _currentRouteStateFlow.value.countIn(location)

    val locationData = location.toLocationData(
        routeId = routeData.routeId,
        distance = routeData.distance
    )

    // update flows
    _currentLocationStateFlow.update { locationData }
    _currentRouteStateFlow.update { routeData }
    _locationsStateFlow.getAndUpdate { locations ->
        locations + listOf(locationData)
    }
}