package sk.lupu.droid.yalu.ui.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import sk.lupu.droid.yalu.ui.components.BottomBar
import sk.lupu.droid.yalu.ui.nav.BottomNavGraph
import sk.lupu.droid.yalu.ui.nav.BottomRouteHolder

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScreen() {
    val navController = rememberNavController()
    Scaffold(
        bottomBar = {
            BottomBar(
                navController = navController,
                bottomRouteHolders = listOf(
                    BottomRouteHolder.History,
                    BottomRouteHolder.Map,
                    BottomRouteHolder.Settings,
                )
            )
        }
    ) { innerPadding ->
        Box(
            modifier = Modifier.padding(
                PaddingValues(0.dp, 0.dp, 0.dp, innerPadding.calculateBottomPadding())
            )
        ) {
            BottomNavGraph(navController = navController)
        }
    }
}
