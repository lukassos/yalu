@file:Suppress("unused")

package sk.lupu.droid.yalu.ui.theme

import androidx.compose.ui.graphics.Color

val primaryColor = Color(0xff4caf50)
val primaryLightColor = Color(0xff80e27e)
val primaryDarkColor = Color(0xff087f23)
val secondaryColor = Color(0xff795548)
val secondaryLightColor = Color(0xffa98274)
val secondaryDarkColor = Color(0xff4b2c20)
val primaryTextColor = Color(0xff000000)
val secondaryTextColor = Color(0xffffffff)

val blackColor = Color(0xff000000)
val whiteColor = Color(0xffffffff)

val greenColor = Color(0xff4caf50)
val greenLightColor = Color(0xff80e27e)
val greenDarkColor = Color(0xff087f23)
val brownColor = Color(0xff795548)
val brownExtraLightColor = Color(0xfffffffb)
val brownLightColor = Color(0xffa98274)
val brownDarkColor = Color(0xff4b2c20)

val redColor = Color(0xffb71c1c)
val redLightColor = Color(0xfff05545)
val redDarkColor = Color(0xff7f0000)

val chartColors = arrayOf(
    redLightColor, greenDarkColor, brownLightColor, greenColor, redDarkColor,
)