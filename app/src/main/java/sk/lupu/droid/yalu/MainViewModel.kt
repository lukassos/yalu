package sk.lupu.droid.yalu

import androidx.compose.runtime.compositionLocalOf
import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.location.FusedLocationProviderClient
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import sk.lupu.droid.yalu.data.datastore.UserSettingsModel
import sk.lupu.droid.yalu.location.getUserLocation
import sk.lupu.droid.yalu.location.handleLocationResults
import sk.lupu.droid.yalu.location.locationFlow
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.model.RouteData

const val TAG = "MainViewModel"

class MainViewModel : ViewModel() {
    internal lateinit var fusedLocationClient: FusedLocationProviderClient
    internal lateinit var settingsFlow: Flow<Preferences>

    private val _isTracking = MutableLiveData(false)
    private val isTracking: LiveData<Boolean>
        get() = _isTracking

    // isTracking producer flow - used by UI to react to
    private val _isTrackingStateFlow = MutableStateFlow(false)
    val isTrackingStateFlow: StateFlow<Boolean> = _isTrackingStateFlow

    private fun isTrackerRunning() = isTracking.value ?: false

    // used in UI to start or stop tracking
    fun toggleLocationTracking(settings: UserSettingsModel) {
        if (!(isTrackerRunning())) {
            // start tracing
            startTracking(settings)
        } else {
            // stop tracking
            stopTracking()
        }
    }

    private fun startTracking(settings: UserSettingsModel) {
        // reinit content flows
        _locationsStateFlow.update { emptyList() }
        _currentLocationStateFlow.update { LocationData.emptyLocationData() }
        _currentRouteStateFlow.update { RouteData.emptyRouteData() }

        // flag tracking state
        _isTracking.postValue(true)
        _isTrackingStateFlow.update { true }
        viewModelScope.launch {
            val flow = fusedLocationClient.locationFlow(settings)
            flow.collect {
                if (isTrackerRunning()) {
                    it.handleLocationResults(
                        _currentLocationStateFlow,
                        _currentRouteStateFlow,
                        _locationsStateFlow
                    )
                }
            }
        }
        viewModelScope.launch {
            settingsFlow.map {

            }.distinctUntilChanged().collect {
                // TODO : make projection and turn odd tracking on interval change ?
            }
        }
    }

    private fun stopTracking() {
        _isTracking.postValue(false)
        _isTrackingStateFlow.update { false }
    }

    fun getUserLocation() {
        fusedLocationClient.getUserLocation(_currentLocationStateFlow)
    }

    // current location producer flow
    private val _currentLocationStateFlow = MutableStateFlow(LocationData.emptyLocationData())
    val currentLocationStateFlow: StateFlow<LocationData> = _currentLocationStateFlow

    // current route producer flow
    private val _currentRouteStateFlow = MutableStateFlow(RouteData.emptyRouteData())
    val currentRouteStateFlow: StateFlow<RouteData> = _currentRouteStateFlow

    // locations producer flow
    private val _locationsStateFlow: MutableStateFlow<List<LocationData>> =
        MutableStateFlow(listOf())
    val locationsStateFlow: StateFlow<List<LocationData>> = _locationsStateFlow
}

val LocalMainViewModel = compositionLocalOf<MainViewModel> { error("$TAG not set") }