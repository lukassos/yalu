package sk.lupu.droid.yalu.data.datastore

import androidx.compose.ui.graphics.Color
import androidx.datastore.preferences.core.Preferences
import com.google.maps.android.compose.MapType

data class UserSettingsModel(
    val trackingInterval: Long,
    val trackingIntervalLabel: String,
    val mapType: MapType,
    val mapLineColor: Color,
    val mapLineColorLabel: String,
    val mapLineWidth: Float, // in pixels
    val mapMarkerInterval: Long,
    val mapMarkerIntervalLabel: String,
    val mapMarkerColor: Color,
    val mapMarkerColorLabel: String,
)

fun Preferences?.toUserSettingsModel(): UserSettingsModel = let{ preferences ->
    if(preferences == null){
        return UserSettingsModel(
            mapType = MapType.NORMAL,
            trackingInterval = SealedPrefs.Settings.DEFAULT_INTERVAL.milliseconds,
            trackingIntervalLabel = SealedPrefs.Settings.DEFAULT_INTERVAL.label,
            mapLineColor = SealedPrefs.Settings.DEFAULT_LINE_COLOR.color,
            mapLineColorLabel = SealedPrefs.Settings.DEFAULT_LINE_COLOR.label,
            mapMarkerInterval = SealedPrefs.Settings.DEFAULT_INTERVAL.milliseconds,
            mapMarkerIntervalLabel = SealedPrefs.Settings.DEFAULT_INTERVAL.label,
            mapMarkerColor = SealedPrefs.Settings.DEFAULT_MARKER_COLOR.color,
            mapMarkerColorLabel = SealedPrefs.Settings.DEFAULT_MARKER_COLOR.label,
            mapLineWidth = SealedPrefs.Settings.DEFAULT_LINE_WIDTH
            )
    }
    val mapMarkerColor = SealedPrefs.Settings.ColorEnum.valueOf(
        preferences[SealedPrefs.Settings.MAP_MARKER_COLOR] ?: SealedPrefs.Settings.DEFAULT_MARKER_COLOR.name
    )
    val mapLineColor = SealedPrefs.Settings.ColorEnum.valueOf(
        preferences[SealedPrefs.Settings.MAP_LINE_COLOR] ?: SealedPrefs.Settings.DEFAULT_LINE_COLOR.name
    )

    val trackingIntervalEnum = SealedPrefs.Settings.IntervalEnum.valueOf(
        preferences[SealedPrefs.Settings.TRACKING_INTERVAL] ?: SealedPrefs.Settings.DEFAULT_INTERVAL.name
    )

    val markerIntervalEnum = SealedPrefs.Settings.IntervalEnum.valueOf(
        preferences[SealedPrefs.Settings.MAP_MARKER_INTERVAL] ?: SealedPrefs.Settings.DEFAULT_INTERVAL.name
    )

    UserSettingsModel(
        mapType = MapType.valueOf(preferences[SealedPrefs.Settings.MAP_TYPE] ?: MapType.NORMAL.name ),
        trackingInterval = trackingIntervalEnum.milliseconds,
        trackingIntervalLabel = trackingIntervalEnum.label,

        mapMarkerInterval = markerIntervalEnum.milliseconds,
        mapMarkerIntervalLabel = markerIntervalEnum.label,
        mapMarkerColor = mapMarkerColor.color,
        mapMarkerColorLabel = mapMarkerColor.label,

        mapLineColor = mapLineColor.color,
        mapLineColorLabel = mapLineColor.label,
        mapLineWidth = preferences[SealedPrefs.Settings.MAP_LINE_WIDTH] ?: SealedPrefs.Settings.DEFAULT_LINE_WIDTH
    )
}