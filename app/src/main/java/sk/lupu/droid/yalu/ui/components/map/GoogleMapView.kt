package sk.lupu.droid.yalu.ui.components.map

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.*
import kotlinx.coroutines.launch
import sk.lupu.droid.yalu.LocalMainViewModel
import sk.lupu.droid.yalu.R
import sk.lupu.droid.yalu.data.datastore.SealedPrefs
import sk.lupu.droid.yalu.data.datastore.toUserSettingsModel
import sk.lupu.droid.yalu.dataStoreSettings
import sk.lupu.droid.yalu.location.filterMarkerPositions
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.model.toLocationData
import sk.lupu.droid.yalu.location.origin
import sk.lupu.droid.yalu.location.toLatLng
import sk.lupu.droid.yalu.location.toLatLngBounds
import sk.lupu.droid.yalu.ui.components.TrackingControls
import sk.lupu.droid.yalu.ui.theme.primaryColor

const val DEFAULT_GOOGLE_MAPS_ZOOM = 4f
const val DEFAULT_GOOGLE_MAPS_BOUNDS_PADDING = 400
const val DEFAULT_ANIMATE_MAP = true

@Composable
fun GoogleMapView(
    modifier: Modifier,
    onMapLoaded: () -> Unit,
    onMarkerClicked: (LocationData) -> Unit,
) {
    val modelView = LocalMainViewModel.current

    val currentLocation by modelView.currentLocationStateFlow.collectAsState(origin)
    val locations by modelView.locationsStateFlow.collectAsState(emptyList())

    val userSettingsState by LocalContext.current.dataStoreSettings.data.collectAsState(initial = null)
    val userSettings = userSettingsState.toUserSettingsModel()

    val shouldAnimateZoom = DEFAULT_ANIMATE_MAP

    val cameraPositionState = rememberCameraPositionState {
        position =
            CameraPosition.fromLatLngZoom(currentLocation.toLatLng(), DEFAULT_GOOGLE_MAPS_ZOOM)
    }
    val coroutineScope = rememberCoroutineScope()

    fun zoomToFit() {
        val bounds =
            if (locations.isNotEmpty()) {
                locations.toLatLngBounds()
            } else {
                modelView.getUserLocation()
                listOf(currentLocation).toLatLngBounds()
            }
        if (shouldAnimateZoom) {
            coroutineScope.launch {
                cameraPositionState.animate(
                    CameraUpdateFactory
                        .newLatLngBounds(bounds, DEFAULT_GOOGLE_MAPS_BOUNDS_PADDING)
                )
            }
        } else {
            cameraPositionState.move(
                CameraUpdateFactory
                    .newLatLngBounds(bounds, DEFAULT_GOOGLE_MAPS_BOUNDS_PADDING)
            )
        }
    }
    GoogleMap(
        modifier = modifier,
        cameraPositionState = cameraPositionState,
        properties = MapProperties(
            mapType = userSettings.mapType,
            isMyLocationEnabled = true,
        ),
        uiSettings = MapUiSettings(
            compassEnabled = false,
            mapToolbarEnabled = false,
            myLocationButtonEnabled = false,
            zoomControlsEnabled = false,
            tiltGesturesEnabled = false,
        ),
        onMapLoaded = {
            zoomToFit()
            onMapLoaded()
        },
        onPOIClick = { /* NO OP consumed */ },
        onMyLocationClick = { location ->
            onMarkerClicked(location.toLocationData())
        }
    ) {
        locations.filterMarkerPositions(
            trackingInterval = userSettings.trackingInterval,
            markerInterval = userSettings.mapMarkerInterval
        ).forEach {
            MapMarker(
                location = it,
                iconResourceId = R.drawable.ic_yalu_marker,
                onClick = { _, locationData -> onMarkerClicked(locationData) },
                tintColor = primaryColor
            )
        }
        MapPolyline(
            locations = locations.map { it.toLatLng() },
            color = userSettings.mapLineColor,
            width = userSettings.mapLineWidth
        )
    }

    Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.SpaceBetween) {
        ZoomControls(
            onZoomOut = {
                if (shouldAnimateZoom) {
                    coroutineScope.launch {
                        cameraPositionState.animate(CameraUpdateFactory.zoomOut())
                    }
                } else {
                    cameraPositionState.move(CameraUpdateFactory.zoomOut())
                }
            },
            onZoomIn = {
                if (shouldAnimateZoom) {
                    coroutineScope.launch {
                        cameraPositionState.animate(CameraUpdateFactory.zoomIn())
                    }
                } else {
                    cameraPositionState.move(CameraUpdateFactory.zoomIn())
                }
            },
            onZoomToFit = {
                zoomToFit()
            }
        )
        TrackingControls()
    }


}

@Composable
fun MapPolyline(
    locations: List<LatLng>,
    color: Color = SealedPrefs.Settings.DEFAULT_LINE_COLOR.color,
    width: Float = SealedPrefs.Settings.DEFAULT_LINE_WIDTH
) {
    Polyline(
        points = locations,
        color = color,
        width = width,
        geodesic = true,
    )
}

