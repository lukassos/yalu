package sk.lupu.droid.yalu.ui.components.map

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun ZoomControls(
    onZoomOut: () -> Unit,
    onZoomIn: () -> Unit,
    onZoomToFit: () -> Unit,
) {
    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
        MapButton("-", onClick = { onZoomOut() })
        MapButton("+", onClick = { onZoomIn() })
        MapButton("Fit", onClick = { onZoomToFit() })
    }
}