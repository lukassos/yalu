package sk.lupu.droid.yalu.ui.screens

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.launch
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.ui.components.LocationDataItem
import sk.lupu.droid.yalu.ui.components.map.GoogleMapView
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun MapScreen() {
    val coroutineScope = rememberCoroutineScope()

    // Map Loaded callback result
    var isMapLoaded by remember { mutableStateOf(false) }

    // Bottom sheet manipulation
    var selectedLocation by remember { mutableStateOf<LocationData?>(null) }
    val bottomSheetScaffoldState = rememberBottomSheetScaffoldState(
        bottomSheetState = BottomSheetState(BottomSheetValue.Collapsed)
    )
    val toggleBottomSheet = {
        coroutineScope.launch {
            if (bottomSheetScaffoldState.bottomSheetState.isCollapsed) {
                bottomSheetScaffoldState.bottomSheetState.expand()
            } else {
                bottomSheetScaffoldState.bottomSheetState.collapse()
            }
        }
    }

    BottomSheetScaffold(
        scaffoldState = bottomSheetScaffoldState,
        sheetContent = {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(200.dp),

                contentAlignment = Alignment.Center
            ) {
                if (selectedLocation == null) {
                    Text("No location data")
                } else {
                    val loc = selectedLocation
                    LocationDataItem(
                        location = loc!!,
                        modifier = Modifier,
                        onClick = { toggleBottomSheet() },
                    )
                }
            }
        },
        sheetPeekHeight = 0.dp
    ) {
        Box(
            modifier = Modifier.fillMaxSize(),
        ) {
            GoogleMapView(
                modifier = Modifier.matchParentSize(),
                onMapLoaded = {
                    isMapLoaded = true
                },
                onMarkerClicked = { location ->
                    selectedLocation = location
                    toggleBottomSheet()
                }
            )
            if (!isMapLoaded) {
                AnimatedVisibility(
                    modifier = Modifier
                        .matchParentSize(),
                    visible = !isMapLoaded,
                    enter = EnterTransition.None,
                    exit = fadeOut()
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier
                            .background(MaterialTheme.colors.background)
                            .wrapContentSize()
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MapScreenPreview() {
    YALUTheme {
        MapScreen()
    }
}
