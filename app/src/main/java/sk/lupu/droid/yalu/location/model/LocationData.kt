package sk.lupu.droid.yalu.location.model

import android.location.Location
import sk.lupu.droid.yalu.location.FUSED_LOCATION_UPDATES_PROVIDER
import sk.lupu.droid.yalu.location.origin

/**
 * Location Update Data Model
 * It is used for each one location update.
 * Used in App ModelView State Flows
 * Used as Location database data model (hence primitives used only)
 */
data class LocationData(
    val routeId: Long, // same as first timestamp of the route

    // cumulative distance at route in meters
    // calculated while tracking 0 otherwise
    val distance: Int,

    // following lines follow android Location data model
    // see: https://developer.android.com/reference/android/location/Location#setAltitude(double)
    val time: Long,
    val latitude: Double,
    val longitude: Double,
    val altitude: Double, // altitudeMeters
    val accuracy: Float, // horizontalAccuracyMeters
    val bearing: Float, // bearingDegrees
    val speed: Float, // m/s
    val provider: String

) {
    companion object {
        fun emptyLocationData() = LocationData(
            routeId = 0L,
            distance = 0,
            time = 0L,
            latitude = origin.latitude,
            longitude = origin.longitude,
            altitude = origin.altitude,
            speed = origin.speed,
            bearing = origin.bearing,
            accuracy = 0f,
            provider = FUSED_LOCATION_UPDATES_PROVIDER
        )
    }
}

fun Location.toLocationData(
    provider: String = FUSED_LOCATION_UPDATES_PROVIDER,
    distance: Int = 0,
    routeId: Long = 0
) = LocationData(
    routeId = if (routeId != 0L) routeId else this.time,
    distance = distance,
    time = this.time,
    latitude = this.latitude,
    longitude = this.longitude,
    altitude = this.altitude,
    accuracy = this.accuracy,
    bearing = this.bearing,
    speed = this.speed,
    provider = provider,
)