package sk.lupu.droid.yalu.location

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.SphericalUtil
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.model.RouteData
import kotlin.math.ceil
import kotlin.math.roundToInt

/**
 * Location to position
 */
fun Location.toLatLng() = LatLng(latitude, longitude)

/**
 * LocationData  to position
 */
fun LocationData.toLatLng() = LatLng(latitude, longitude)

/**
 * Calculate boundary for list of position coordinates
 * Path to Boundary
 */
fun List<LocationData>.toLatLngBounds(): LatLngBounds {
    val locations = this
    return LatLngBounds.Builder().apply {
        locations.forEach { include(it.toLatLng()) }
    }.build()
}

/**
 * Recalculate boundary with new position coordinate
 * Boundary + position = new Boundary
 */
@Suppress("unused") // might be used in next iteration
fun LatLngBounds.add(position: LatLng): LatLngBounds {
    val bounds = this
    return LatLngBounds.Builder().apply {
        include(bounds.northeast)
        include(bounds.southwest)
        include(position)
    }.build()
}

@Suppress("ConvertTwoComparisonsToRangeCheck") // <<--- personally find this more readable than 1 until something
fun List<LocationData>.filterMarkerPositions(
    markerInterval: Long,
    trackingInterval: Long,
): List<LocationData> {
    val n = if (markerInterval > trackingInterval && trackingInterval > 0) {
        ceil(markerInterval.toDouble() / trackingInterval).toInt()
    } else 1
    // we could use slice() as well but then we would get list sliced by mod
    // but then we would get even the last point every so often, which is not best looking
    return chunked(n).filter { it.size == n }.map { it[0] }
}

/**
 * spherical Distance in Meters
 */
fun LatLng.distance(nextLocation: LatLng) =
    SphericalUtil.computeDistanceBetween(
        this,
        nextLocation

    ).roundToInt()

/**
 * spherical Distance in Meters
 */
fun LocationData.distance(nextLocation: LocationData) =
    toLatLng().distance(
        nextLocation.toLatLng()
    )

/**
 * spherical Distance in Meters
 */
fun RouteData.distance(nextLocation: LocationData) =
    LatLng(endLat, endLng).distance(
        nextLocation.toLatLng()
    )

/**
 * spherical Distance in Meters
 */
fun RouteData.distance(nextLocation: Location) =
    LatLng(endLat, endLng).distance(
        nextLocation.toLatLng()
    )

fun List<LocationData>.findMinAccuracy()= map{it.accuracy}.takeIf { it.isNotEmpty() }?.reduce { a, b -> a.coerceAtMost(b) }

fun List<LocationData>.findMaxAccuracy()= map{it.accuracy}.takeIf { it.isNotEmpty() }?.reduce { a, b -> a.coerceAtLeast(b) }
