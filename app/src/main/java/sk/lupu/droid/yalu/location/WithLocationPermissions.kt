package sk.lupu.droid.yalu.location

import android.os.Build
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun WithLocationPermissions(
    onSuccess: @Composable ()->Unit
) {
    val locationPermissionsState =
        rememberMultiplePermissionsState(
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                listOf(
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
//                    android.Manifest.permission.ACCESS_BACKGROUND_LOCATION, // TODO : BUG - bg location was not really requested and app crashed
                )
            } else {
                listOf(
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                )
            }
        )

    if (locationPermissionsState.allPermissionsGranted) {
        onSuccess()
    } else {
        // TODO : make this visually appealing :)
        Column(
            modifier = Modifier.fillMaxSize().padding( 18.dp ),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,

        ){
            val allPermissionsRevoked =
                locationPermissionsState.permissions.size ==
                        locationPermissionsState.revokedPermissions.size

            val textToShow = if (!allPermissionsRevoked) {
                // If not all the permissions are revoked, it's because the user accepted the COARSE
                // location permission, but not the FINE one.
                "YALU cannot work without location permissions. " +
                "Thanks for approximate location. " +
                        "\nPlease grant us access to your location."
            } else if (locationPermissionsState.shouldShowRationale) {
                // Both location permissions have been denied
                "YALU cannot work without location permissions. " +
                        "\nPlease grant us access to your location."
            } else {
                // First time the user sees this feature or the user doesn't want to be asked again
                "YALU cannot work without location permissions. " +
                "\nPlease grant us access to your location."
            }

            val buttonText = if (!allPermissionsRevoked) {
                "Allow precise location"
            } else {
                "Request location "
            }

            Text(text = textToShow, overflow = TextOverflow.Ellipsis)
            Spacer(modifier = Modifier.height(8.dp))
            Button(onClick = { locationPermissionsState.launchMultiplePermissionRequest() }) {
                Text(buttonText)
            }
        }
    }
}