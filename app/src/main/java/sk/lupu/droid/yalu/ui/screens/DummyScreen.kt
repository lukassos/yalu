package sk.lupu.droid.yalu.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun DummyScreen(name: String, bgColor:Color = Color.DarkGray) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(bgColor),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = "Screen $name!",
            style = MaterialTheme.typography.h3,
            fontWeight = FontWeight.Light,
            color = if (bgColor == Color.Green) Color.Black else Color.Green

        )
    }

}

@Preview(showBackground = true)
@Composable
fun DummyScreenPreview() {
    YALUTheme {
        DummyScreen("Preview")
    }
}