package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun SimpleInfo(label: String, value: String) {
    Row {
        Text(
            text = label,

            style = MaterialTheme.typography.body2,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .weight(1f)
                .padding(bottom = 4.dp)
        )
        Text(
            text = value,
            style = MaterialTheme.typography.body2,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            modifier = Modifier
                .weight(1f)
                .padding(bottom = 4.dp)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun SimpleInfoPreview() {
    YALUTheme {
        SimpleInfo(label = "Lorem", value = "ipsum")
    }
}