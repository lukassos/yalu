package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.location.model.RouteData
import sk.lupu.droid.yalu.location.model.toRouteData
import sk.lupu.droid.yalu.location.origin
import sk.lupu.droid.yalu.ui.theme.YALUTheme
import sk.lupu.droid.yalu.ui.toElapsedTime
import sk.lupu.droid.yalu.ui.toReadableDistance

@Composable
fun TrackingInfoItem(
    route: RouteData,
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(
        start = 8.dp,
        top = 8.dp,
        end = 8.dp,
        bottom = 8.dp
    ),
    shape: Shape = RectangleShape,
    elevation: Dp = 6.dp,
    onClick: () -> Unit = {}
) {
    Surface(
        elevation = elevation,
        shape = shape,
        modifier = Modifier.padding(paddingValues)
    ) {
        Row(modifier = Modifier.clickable(onClick = onClick)) {
            Column(
                modifier = Modifier.padding(
                    start = 16.dp,
                    top = 16.dp,
                    end = 16.dp,
                    bottom = 8.dp
                )
            ) {
                Text(text = "Tracking...", fontStyle = MaterialTheme.typography.h4.fontStyle)

                SimpleInfo("Elapsed Time", route.elapsedTime.toElapsedTime())
                SimpleInfo("Distance", route.distance.toReadableDistance())
                SimpleInfo("Avg Speed", "${route.avgSpeed} m/s")

            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TrackingInfoItemPreview() {
    YALUTheme {
        RouteDataItem(route = origin.toRouteData()) {
            /* noop */
        }
    }
}