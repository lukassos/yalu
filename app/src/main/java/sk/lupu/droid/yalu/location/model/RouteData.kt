package sk.lupu.droid.yalu.location.model

import android.location.Location
import android.util.Log
import sk.lupu.droid.yalu.location.distance
import kotlin.math.max
import kotlin.math.min

/**
 * Location Update Data Model
 * It is used for each one location update.
 * Used in App ModelView State Flows
 * Used as Location Route database data model (hence primitives used only)
 */
data class RouteData(
    val routeId: Long, // same as first timestamp of the route
    val elapsedTime: Long, // in millis
    // cumulative distance at route in meters
    // calculated while tracking 0 otherwise
    val distance: Int,

    val avgSpeed: Float, // m/s
    val minSpeed: Float, // m/s
    val maxSpeed: Float, // m/s
    val minAlt: Double, // m
    val maxAlt: Double, // m

    val startLat: Double,
    val startLng: Double,
    val endLat: Double,
    val endLng: Double,
) {
    companion object {
        fun emptyRouteData() = RouteData(
            routeId = 0L,
            elapsedTime = 0L, // in millis
            distance = 0,
            avgSpeed = 0f, // m/s

            minSpeed = Float.MAX_VALUE, // m/s
            maxSpeed = Float.MIN_VALUE, // m/s
            minAlt = Double.MAX_VALUE, // m
            maxAlt = Double.MIN_VALUE, // m

            startLat = Double.MIN_VALUE, // invalid value
            startLng = Double.MIN_VALUE,// invalid value
            endLat = Double.MIN_VALUE,// invalid value
            endLng = Double.MIN_VALUE,// invalid value
        )
    }
}

fun LocationData.toRouteData() = RouteData(
    routeId = routeId,
    elapsedTime = time - routeId,
    distance = distance,
    avgSpeed = 0f, // m/s

    minSpeed = Float.MAX_VALUE, // m/s
    maxSpeed = Float.MIN_VALUE, // m/s
    minAlt = Double.MAX_VALUE, // m
    maxAlt = Double.MIN_VALUE, // m

    startLat = Double.NEGATIVE_INFINITY, // invalid value
    startLng = Double.NEGATIVE_INFINITY,// invalid value
    endLat = Double.NEGATIVE_INFINITY,// invalid value
    endLng = Double.NEGATIVE_INFINITY,// invalid value
)

fun RouteData.countIn(location: Location): RouteData  {
    Log.e("TAG", ": 1sdfasdf distance ${this.distance(location).takeIf { it < 5000 }?:0}")
    Log.e("TAG", ": 2sdfasdf distance ${distance.plus(this.distance(location).takeIf { it < 5000 }?:0 )}")

    return RouteData(
        routeId = if (routeId != 0L) routeId else location.time,
        elapsedTime = location.time - routeId,
        distance = distance.plus(this.distance(location).takeIf { it < 5000 }?:0 ),

        avgSpeed = distance.toDouble().div(elapsedTime ).times(1000).toFloat(), // m/s
        minSpeed = min(minSpeed, location.speed),
        maxSpeed = max(maxSpeed, location.speed),

        minAlt = min(minAlt, location.altitude),
        maxAlt = max(maxAlt, location.altitude),

        startLat = if (startLat == Double.NEGATIVE_INFINITY) location.latitude else startLat,
        startLng = if (startLng == Double.NEGATIVE_INFINITY) location.longitude else startLng,
        endLat = location.latitude,
        endLng = location.longitude,
    )
}