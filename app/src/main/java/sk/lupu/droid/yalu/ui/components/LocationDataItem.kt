package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.origin
import sk.lupu.droid.yalu.ui.theme.YALUTheme
import sk.lupu.droid.yalu.ui.toReadableDistance
import sk.lupu.droid.yalu.ui.toReadableTime

@Composable
fun LocationDataItem(
    location: LocationData,
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(
        start = 8.dp,
        top = 8.dp,
        end = 8.dp,
        bottom = 8.dp
    ),
    shape: Shape = RectangleShape,
    elevation: Dp = 6.dp,
    onClick: () -> Unit = {}
) {
    Surface(
        elevation = elevation,
        shape = shape,
        modifier = Modifier.padding(paddingValues).defaultMinSize(400.dp, 200.dp)
    ) {
        Row(modifier = Modifier.clickable(onClick = onClick)) {
            Column(
                modifier = Modifier.padding(
                    start = 16.dp,
                    top = 16.dp,
                    end = 16.dp,
                    bottom = 8.dp
                )
            ) {
                if (location.routeId > 0L) SimpleInfo(
                    "Start Time",
                    location.routeId.toReadableTime()
                )
                SimpleInfo("Time", location.time.toReadableTime())
                SimpleInfo("Distance", location.distance.toReadableDistance())
                SimpleInfo("Longitude", String.format("%.5f", location.longitude))
                SimpleInfo("Latitude", String.format("%.5f", location.latitude))
                SimpleInfo("Altitude", "${String.format("%.2f",location.altitude)} m")
                SimpleInfo("Accuracy", "${String.format("%.2f",location.accuracy)} m")
                SimpleInfo("Speed", "${String.format("%.2f",location.speed)} m/s")
                SimpleInfo("Bearing", "${String.format("%.2f",location.bearing)}°")
                SimpleInfo("Provider", location.provider)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LocationDataItemPreview() {
    YALUTheme {
        LocationDataItem(location = origin) {
            /* noop */
        }
    }
}