package sk.lupu.droid.yalu.ui.components.map

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun MapIconButton(
    imageVector: ImageVector,
    contentDescription: String,
    paddingValues: PaddingValues = PaddingValues(0.dp),
    color: Color = MaterialTheme.colors.primary,
    bgColor: Color = MaterialTheme.colors.secondary,
    onClick: () -> Unit
) {
    IconButton(
//        color = ButtonDefaults.buttonColors(
//            backgroundColor = MaterialTheme.colors.onPrimary,
//            contentColor = MaterialTheme.colors.primary
//        ),
        onClick = onClick,
        modifier = Modifier
            .padding(paddingValues)
            .then(Modifier.size(50.dp))
            .border(1.dp, color, shape = CircleShape)
            .background(bgColor, shape = CircleShape)
    ) {
        Icon(
            imageVector = imageVector,
            contentDescription = contentDescription,
            tint = color
        )
    }
}

@Preview
@Composable
fun MapIconButtonPreview() {
    YALUTheme {
        MapIconButton(
            Icons.Default.ThumbUp,
            "Hello thumbs up",
        ) {
            /* No op */
        }
    }
}