package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.location.findMaxAccuracy
import sk.lupu.droid.yalu.location.findMinAccuracy
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.origin
import sk.lupu.droid.yalu.location.origin2
import sk.lupu.droid.yalu.ui.theme.YALUTheme
import sk.lupu.droid.yalu.ui.theme.brownExtraLightColor
import sk.lupu.droid.yalu.ui.theme.chartColors


// TODO :  WIP - align to bottom and color the columns
@Composable
fun LocationAccuracyBarChartItem(
    locations: List<LocationData>,
    modifier: Modifier = Modifier,
    takeN: Int = 10,
    paddingValues: PaddingValues = PaddingValues(
        start = 8.dp,
        top = 8.dp,
        end = 8.dp,
        bottom = 8.dp
    ),
    height: Dp = 120.dp,
    minColHeight: Dp= 20.dp,
    maxColHeight: Dp = 100.dp,
    shape: Shape = RectangleShape,
    elevation: Dp = 6.dp,
    onClick: () -> Unit = {}
) {
    val sourceLocations = locations.takeLast(10)
    val min = sourceLocations.findMinAccuracy()?:0f
    val max = sourceLocations.findMaxAccuracy()?:100f
    val maxMin = (max - min).takeIf { it != 0f }?: 1f
    val colH = maxColHeight.value-minColHeight.value

    Surface(
        elevation = elevation,
        shape = shape,
        modifier = Modifier.padding(paddingValues)
    ) {
        LazyRow(
            modifier = Modifier,
            verticalAlignment = Alignment.Bottom

        ) {
            itemsIndexed(sourceLocations) { i, location ->
                val normValue = location.accuracy.minus(min).div(maxMin)
                val calcHeight = normValue.times(colH).plus(minColHeight.value)
                val colHeightDp = Dp(calcHeight)
                val colLabel = "${ String.format("%.2f", location.accuracy) } m"
                val color = chartColors[i.rem(chartColors.size)]
                Column(
                    modifier = Modifier
                        .padding(
                            start = 16.dp,
                            top = 16.dp,
                            end = 16.dp,
                            bottom = 8.dp
                        )
                        .height(height)
                        .wrapContentHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Bottom

                    ) {
                    Box(
                        modifier = Modifier
                            .size(8.dp, colHeightDp)
                            .padding(
                                start = 16.dp,
                                top = 16.dp,
                                end = 16.dp,
                                bottom = 8.dp
                            )
                            .background(
                                color = chartColors[i.rem(chartColors.size)],
                                shape = RoundedCornerShape(4.dp)
                            )
                            .border(
                                width = 2.dp, color = brownExtraLightColor,
                                shape = RoundedCornerShape(4.dp)
                            )
                            .shadow(2.dp),
                    ){
                        Text(
                            text = "",
                            fontSize = MaterialTheme.typography.caption.fontSize,
                        )
                    }
                    Text(
                        text = colLabel,
                        fontSize = MaterialTheme.typography.caption.fontSize,
                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun LocationAccuracyBarChartItemPreview() {
    YALUTheme {
        val list = listOf(origin, origin2, origin2, origin, origin2)
        LocationAccuracyBarChartItem(locations = list) {
            /* noop */
        }
    }
}