package sk.lupu.droid.yalu.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.LocalMainViewModel
import sk.lupu.droid.yalu.ui.components.LocationDataItem
import sk.lupu.droid.yalu.ui.components.RouteDataItem
import sk.lupu.droid.yalu.ui.theme.YALUTheme


@Composable
fun HistoryScreen() {
    val modelView = LocalMainViewModel.current
    val locations by modelView.locationsStateFlow.collectAsState(emptyList())
    val route by modelView.currentRouteStateFlow.collectAsState()

    val titleFontStyle = MaterialTheme.typography.h3.fontStyle

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
        contentAlignment = Alignment.Center
    ) {
        if (locations.isEmpty() && route.routeId == 0L) {
            // no data clause
            Column(verticalArrangement = Arrangement.SpaceAround, horizontalAlignment = Alignment.CenterHorizontally) {
                Text("No Data", fontStyle = titleFontStyle)
                Text("Start tracking in map view first")
            }
        }else{

            Column(verticalArrangement = Arrangement.spacedBy(10.dp)) {
                Text(text = "Route info", fontStyle = titleFontStyle)
                RouteDataItem(route = route)
                // TODO: BAR CHART comes here


//                Text(text = "Accuracy", fontStyle = titleFontStyle)

//                LocationAccuracyBarChartItem(locations = locations) // TODO: uncomment for unfinished bar chart

                Text(text = "Locations", fontStyle = titleFontStyle)

                LazyColumn {
                    items(locations) { location ->
                        LocationDataItem(location) {}
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HistoryScreenPreview() {
    YALUTheme {
        HistoryScreen()
    }
}
