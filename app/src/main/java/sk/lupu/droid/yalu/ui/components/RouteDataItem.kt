package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.location.model.RouteData
import sk.lupu.droid.yalu.location.model.toRouteData
import sk.lupu.droid.yalu.location.origin
import sk.lupu.droid.yalu.ui.theme.YALUTheme
import sk.lupu.droid.yalu.ui.toElapsedTime
import sk.lupu.droid.yalu.ui.toReadableDistance
import sk.lupu.droid.yalu.ui.toReadableTime

@Composable
fun RouteDataItem(
    route: RouteData,
    modifier: Modifier = Modifier,
    paddingValues: PaddingValues = PaddingValues(
        start = 8.dp,
        top = 8.dp,
        end = 8.dp,
        bottom = 8.dp
    ),
    shape: Shape = RectangleShape,
    elevation: Dp = 6.dp,
    onClick: () -> Unit = {}
) {
    Surface(
        elevation = elevation,
        shape = shape,
        modifier = Modifier.padding(paddingValues)
    ) {
        Row(modifier = Modifier.clickable(onClick = onClick)) {
            Column(
                modifier = Modifier.padding(
                    start = 16.dp,
                    top = 16.dp,
                    end = 16.dp,
                    bottom = 8.dp
                )
            ) {

                SimpleInfo("Start Time", route.routeId.toReadableTime())
                SimpleInfo("Elapsed Time", route.elapsedTime.toElapsedTime())
                SimpleInfo("Distance", route.distance.toReadableDistance())
                SimpleInfo("Min Speed", "${String.format("%.2f",route.minSpeed)} m/s")
                SimpleInfo("Max Speed", "${String.format("%.2f",route.maxSpeed)} m/s")
                SimpleInfo("Avg Speed", "${String.format("%.2f",route.avgSpeed)} m/s")
                SimpleInfo("Min Altitude", "${String.format("%.2f",route.minAlt)} m")
                SimpleInfo("Max Altitude", "${String.format("%.2f",route.maxAlt)} m")

            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun RouteDataItemPreview() {
    YALUTheme {
        RouteDataItem(route = origin.toRouteData()){
            /* noop */
        }
    }
}