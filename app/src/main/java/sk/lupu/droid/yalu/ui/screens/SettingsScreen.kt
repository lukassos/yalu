package sk.lupu.droid.yalu.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.google.maps.android.compose.MapType
import com.jamal.composeprefs.ui.PrefsScreen
import com.jamal.composeprefs.ui.prefs.DropDownPref
import com.jamal.composeprefs.ui.prefs.SliderPref
import sk.lupu.droid.yalu.data.datastore.SealedPrefs.Settings
import sk.lupu.droid.yalu.data.datastore.SealedPrefs.Settings.DEFAULT_LINE_WIDTH
import sk.lupu.droid.yalu.data.datastore.SealedPrefs.Settings.IntervalEnum
import sk.lupu.droid.yalu.data.datastore.SealedPrefs.Settings.toDropdownChoice
import sk.lupu.droid.yalu.data.datastore.toUserSettingsModel
import sk.lupu.droid.yalu.dataStoreSettings
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun SettingsScreen() {
    val prefs = LocalContext.current.dataStoreSettings
    val userSettingsState by prefs.data.collectAsState(initial = null)
    val userSettings = userSettingsState.toUserSettingsModel()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
        verticalArrangement = Arrangement.Top
    ) {
        SettingsPrefs(prefs)
    }

}

@Preview(showBackground = true)
@Composable
fun SettingsScreenPreview() {
    YALUTheme {
        SettingsScreen()
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SettingsPrefs(prefs: DataStore<Preferences>) {
    val userSettingsState by prefs.data.collectAsState(initial = null)
    val userSettings = userSettingsState.toUserSettingsModel()
    PrefsScreen(dataStore = prefs) {
        prefsGroup("Tracking") {
//            prefsItem {
//                SwitchPref(
//                    key = Settings.TRACKING_TOGGLE.name,
//                    title = "Tracking"
//                )
//            }
//            prefsItem {
//                SwitchPref(
//                    key = Settings.TRACKING_USE_FINE_LOCATION_UPDATES.name,
//                    title = "Use fine location updates"
//                )
//            }
            prefsItem {
                DropDownPref(
                    key = Settings.TRACKING_INTERVAL.name,
                    title = "Tracking interval",
                    summary = "Make location update every ${userSettings.trackingIntervalLabel}",
                    entries = IntervalEnum.values().associate { it.toDropdownChoice() }
                )
            }
        }
        prefsGroup("Map Settings") {
            prefsItem {
                DropDownPref(
                    key = Settings.MAP_TYPE.name,
                    title = "Map Terrain",
                    summary = "Using ${userSettings.mapType.name}",
                    entries = mapOf(
                        MapType.NORMAL.toDropdownChoice(),
                        MapType.NONE.toDropdownChoice(),
                        MapType.SATELLITE.toDropdownChoice(),
                        MapType.TERRAIN.toDropdownChoice(),
                        MapType.HYBRID.toDropdownChoice(),
                    )
                )
            }

        }
        prefsGroup("Drawing") {

// TODO : TINT_MARKER : marker tinting is kinda hard .. easier is to make more markers like Google, Yalu, Sygic
//            prefsItem {
//                DropDownPref(
//                    key = Settings.MAP_MARKER_COLOR.name,
//                    title = "Marker color",
//                    summary = "Tint the marker with ${userSettings.mapMarkerColorLabel}",
//                    entries = Settings.ColorEnum.values().associate { it.toDropdownChoice() }
//                )
//            }

            prefsItem {
                DropDownPref(
                    key = Settings.MAP_MARKER_INTERVAL.name,
                    title = "Marker interval",
                    summary = "Create marker after ${userSettings.mapMarkerIntervalLabel}",
                    entries = IntervalEnum.values().associate { it.toDropdownChoice() }
                )
            }
            prefsItem {
                SliderPref(
                    key = Settings.MAP_LINE_WIDTH.name,
                    title = "Polyline width",
                    valueRange = 5f..20f,
                    steps = 5,
                    showValue = true,
                    defaultValue = DEFAULT_LINE_WIDTH
                )
            }
            prefsItem {
                DropDownPref(
                    key = Settings.MAP_LINE_COLOR.name,
                    title = "Polyline color",
                    summary = "Tint the marker with ${userSettings.mapLineColorLabel} ",
                    entries = Settings.ColorEnum.values().associate { it.toDropdownChoice() }
                )
            }
        }
    }
}
