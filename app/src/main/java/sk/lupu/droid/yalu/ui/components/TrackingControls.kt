package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.LocalMainViewModel
import sk.lupu.droid.yalu.data.datastore.toUserSettingsModel
import sk.lupu.droid.yalu.dataStoreSettings
import sk.lupu.droid.yalu.ui.components.map.MapIconButton
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun TrackingControls(
) {
    val userSettingsState by LocalContext.current.dataStoreSettings.data.collectAsState(initial = null)
    val userSettings = userSettingsState.toUserSettingsModel()
    val modelView = LocalMainViewModel.current
    val currentLocation by modelView.currentLocationStateFlow.collectAsState()
    val route by modelView.currentRouteStateFlow.collectAsState()
    val isTracking by modelView.isTrackingStateFlow.collectAsState()

    var isInfoCollapsed by remember { mutableStateOf(true) }
    fun toggleInfoCollapse() {
        isInfoCollapsed = !isInfoCollapsed
    }

    Box {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
            if (isTracking) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.End
                ) {
                    if (!isInfoCollapsed) {

                        LocationDataItem(
                            modifier = Modifier
                                .fillMaxWidth(),
                            location = currentLocation
                        ) {
                            toggleInfoCollapse()
                        }

                        RouteDataItem(route = route) {
                            toggleInfoCollapse()
                        }
                    }
                    TrackingInfoItem(route = route) {
                        toggleInfoCollapse()
                    }
                }
            }

        }
//        Column(Modifier.wrapContentSize(), horizontalAlignment = Alignment.End) {
        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.Top,
            horizontalArrangement = Arrangement.End
        ) {
            MapIconButton(
                paddingValues = PaddingValues(4.dp),
                imageVector = if (isTracking) Icons.Default.Done else Icons.Default.PlayArrow,
                contentDescription = if (isTracking) "Stop tracking icon" else "Start tracking icon",
                onClick = {
                    modelView.toggleLocationTracking(userSettings)
                }
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TrackingControlsPreview() {
    YALUTheme {
        TrackingControls()
    }
}

