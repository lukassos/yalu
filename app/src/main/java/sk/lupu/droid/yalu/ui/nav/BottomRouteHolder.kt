package sk.lupu.droid.yalu.ui.nav

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Settings
import androidx.compose.ui.graphics.vector.ImageVector

sealed class BottomRouteHolder (
    val route: String,
    val title: String,
    val icon: ImageVector,
){
    object Map: BottomRouteHolder(
        route = "map",
        title = "Map",
        icon = Icons.Default.LocationOn
    )
    object History: BottomRouteHolder(
        route = "history",
        title = "History",
        icon = Icons.Default.List
    )
    object Settings: BottomRouteHolder(
        route = "settings",
        title = "Setup",
        icon = Icons.Default.Settings
    )
}
