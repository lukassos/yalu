package sk.lupu.droid.yalu.ui

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue

@SuppressLint("SimpleDateFormat")
fun Long.toReadableTime(): String {
    val timeD = Date(this)
    val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

    return sdf.format(timeD)
}

fun Int.toReadableDistance(): String = "$this m"

@SuppressLint("DefaultLocale") // at the moment only one locale is used
fun Long.toElapsedTime(startTime:Long = 0L): String {
    val diffTime = this.minus(startTime).absoluteValue
    return java.lang.String.format(
        "%02d:%02d:%02d",
        TimeUnit.MILLISECONDS.toHours(diffTime),
        TimeUnit.MILLISECONDS.toMinutes(diffTime) % TimeUnit.HOURS.toMinutes(1),
        TimeUnit.MILLISECONDS.toSeconds(diffTime) % TimeUnit.MINUTES.toSeconds(1)
    )

}
