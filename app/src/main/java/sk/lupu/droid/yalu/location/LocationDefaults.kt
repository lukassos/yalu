package sk.lupu.droid.yalu.location

import sk.lupu.droid.yalu.location.model.LocationData

const val FUSED_LOCATION_UPDATES_PROVIDER = "fused"

// work place approx = 48.19755153554471, 17.039526392638027, alt 188
// initial coordinate and location data
val origin = LocationData(
    routeId = 0L,
    distance = 0,
    time = 0L,
    latitude = 48.19755153554471,
    longitude = 17.039526392638027,
    altitude = 188.0,
    accuracy = 16f,
    bearing = 0f,
    speed = 0f,
    provider = FUSED_LOCATION_UPDATES_PROVIDER
)
val origin2 = LocationData(
    routeId = 0L,
    distance = 0,
    time = 0L,
    latitude = 48.19755,
    longitude = 17.0395,
    altitude = 188.0,
    accuracy = 105f,
    bearing = 0f,
    speed = 0f,
    provider = FUSED_LOCATION_UPDATES_PROVIDER
)
