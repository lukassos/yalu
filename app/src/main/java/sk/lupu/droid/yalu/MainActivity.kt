package sk.lupu.droid.yalu

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.google.android.gms.location.LocationServices
import sk.lupu.droid.yalu.data.datastore.SealedPrefs
import sk.lupu.droid.yalu.location.WithLocationPermissions
import sk.lupu.droid.yalu.ui.screens.DummyScreen
import sk.lupu.droid.yalu.ui.screens.MainScreen
import sk.lupu.droid.yalu.ui.theme.YALUTheme

val Context.dataStoreSettings: DataStore<Preferences> by preferencesDataStore(name = SealedPrefs.Settings.name)

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewModel: MainViewModel by viewModels()
        viewModel.fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        viewModel.settingsFlow = dataStoreSettings.data

        setContent {
            YALUTheme {
                WithLocationPermissions {
                    viewModel.getUserLocation() // used for initial map view position
                    CompositionLocalProvider(
                        LocalMainViewModel provides viewModel
                    ) {
                        Surface(
                            modifier = Modifier.fillMaxSize(),
                            color = MaterialTheme.colors.background
                        ) {
                            MainScreen()
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    YALUTheme {
        DummyScreen("Android")
    }
}



