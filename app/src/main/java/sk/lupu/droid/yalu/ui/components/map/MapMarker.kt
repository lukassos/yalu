package sk.lupu.droid.yalu.ui.components.map

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import sk.lupu.droid.yalu.location.model.LocationData
import sk.lupu.droid.yalu.location.toLatLng
import sk.lupu.droid.yalu.ui.theme.primaryColor

private const val TAG = "MapMarker"

@Composable
fun MapMarker(
    location: LocationData,
    @DrawableRes iconResourceId: Int?,
    tintColor: Color = primaryColor,
    onClick: (Marker, LocationData) -> Unit
) {
    val markerClick: (Marker) -> Boolean = { marker ->
        Log.d(TAG, "${marker.title} was clicked")
        onClick.invoke( marker, location )
        false
    }

    if(iconResourceId == null){
        Marker(
            state = MarkerState(position = location.toLatLng()),
            onClick = markerClick
        )
    }else {
        val icon = bitmapDescriptor(
            LocalContext.current, iconResourceId,
        )
        Marker(
            state = MarkerState(position = location.toLatLng()),
            icon = icon,
            onClick = markerClick
        )
    }
}

fun bitmapDescriptor(
    context: Context,
    vectorResId: Int
): BitmapDescriptor? {

    // retrieve the actual drawable
    val drawable = ContextCompat.getDrawable(context, vectorResId) ?: return null
    drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
    val bm = Bitmap.createBitmap(
        drawable.intrinsicWidth,
        drawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )

    // draw it onto the bitmap
    val canvas = android.graphics.Canvas(bm)
    drawable.draw(canvas)
    return BitmapDescriptorFactory.fromBitmap(bm)
}


//TODO :try to tint the marker
//https://stackoverflow.com/questions/36849164/tint-a-google-maps-android-api-marker
//private static Paint markerPaint;
//private static Paint whitePaint;
//
//Bitmap markerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mapmarker);
//Bitmap resultBitmap = Bitmap.createBitmap(markerBitmap, 0, 0, markerBitmap.getWidth() - 1, markerBitmap.getHeight() - 1);
//ColorFilter filter = new PorterDuffColorFilter(Themer.getPrimaryColor(getActivity()), PorterDuff.Mode.SRC_IN);
//if (markerPaint == null) {
//    markerPaint = new Paint();
//    markerPaint.setColorFilter(filter);
//} else {
//    markerPaint.setColorFilter(filter);
//}
//Canvas canvas = new Canvas(resultBitmap);
//canvas.drawBitmap(resultBitmap, 0, 0, markerPaint);
//if (Themer.shouldShowStopCounts(getActivity())) {
//    if (whitePaint == null) {
//        whitePaint = new Paint();
//        whitePaint.setColor(Color.WHITE);
//        whitePaint.setTextSize(40f);
//        whitePaint.setTextAlign(Paint.Align.CENTER);
//    }
//    canvas.drawText(item.name, resultBitmap.getWidth() / 2, resultBitmap.getHeight() / 2, whitePaint);
//}
//markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resultBitmap));