package sk.lupu.droid.yalu.ui.screens

import androidx.compose.ui.graphics.Color
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import sk.lupu.droid.yalu.location.model.LocationData

data class MapCurrentPositionData(
    val isTracking: Boolean,
    val position: LatLng,
    val startTime: String,
    val distance: Int,
)

data class MapPathData(
    val isTracking: Boolean,
    val width: Float,
    val color: Color,
    val positions: List<LatLng>,
)

data class MapMarkersData(
    val isTracking: Boolean,
    val color: Color,
    val locations: List<LocationData>,
)

data class MapFitBoundsData(
    val bounds: LatLngBounds
)

