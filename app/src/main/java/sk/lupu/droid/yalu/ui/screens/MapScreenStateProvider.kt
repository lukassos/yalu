package sk.lupu.droid.yalu.ui.screens

import androidx.compose.runtime.Composable
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.flow.combine
import sk.lupu.droid.yalu.MainViewModel
import sk.lupu.droid.yalu.data.datastore.toUserSettingsModel
import sk.lupu.droid.yalu.location.filterMarkerPositions
import sk.lupu.droid.yalu.location.origin
import sk.lupu.droid.yalu.location.toLatLng
import sk.lupu.droid.yalu.location.toLatLngBounds
import sk.lupu.droid.yalu.ui.toReadableTime


@Suppress("UNUSED_VARIABLE") // TODO : planned to be used in next impl iteration
@Composable
fun MapScreenStateController(
    locationProvider: MainViewModel,
    dataStoreSettings: DataStore<Preferences>
) {
    val settingsFlow = dataStoreSettings.data

    val isTrackingFlow = locationProvider.isTrackingStateFlow

    // current position and ui texts
    val mapCurrentPositionDataFlow = combine(
        locationProvider.isTrackingStateFlow,
        locationProvider.currentLocationStateFlow
    ) { isTracking, currentLocation ->

        MapCurrentPositionData(
            isTracking = isTracking,
            distance = currentLocation.distance,
            startTime = currentLocation.routeId.toReadableTime(),
            position = currentLocation.toLatLng()
        )
    }

    // path drawing
    val mapPathDataFlow = combine(
        locationProvider.isTrackingStateFlow,
        locationProvider.locationsStateFlow,
        settingsFlow
    ) { isTracking, locations, userPrefs ->
        val userSettingsModel = userPrefs.toUserSettingsModel()
        MapPathData(
            isTracking = isTracking,
            positions = locations.map { it.toLatLng() },
            color = userSettingsModel.mapLineColor,
            width = userSettingsModel.mapLineWidth,
        )
    }

    // markers drawing
    val mapMarkersDataFlow = combine(
        locationProvider.isTrackingStateFlow,
        locationProvider.locationsStateFlow,
        settingsFlow
    ) { isTracking, locations, userPrefs ->
        val userSettingsModel = userPrefs.toUserSettingsModel()
        val trackingInterval = userSettingsModel.trackingInterval
        val markerInterval = userSettingsModel.mapMarkerInterval
        MapMarkersData(
            isTracking = isTracking,
            locations = locations.filterMarkerPositions(
                markerInterval,
                trackingInterval
            ), // NOTE TO SELF : simpler is to get it with slice but then two markers will appear at current location
            color = userSettingsModel.mapMarkerColor,
        )
    }

    // zoom to fit boundaries from incremental updates
    val mapFitBoundsDataFlow = combine(
        locationProvider.isTrackingStateFlow,
        locationProvider.locationsStateFlow
    ) { isTracking, locations ->
        if (isTracking) {
            MapFitBoundsData(
                bounds = listOf(origin).toLatLngBounds()
            )
        } else {
            MapFitBoundsData(
                bounds = locations.toLatLngBounds()
            )
        }
    }
}
