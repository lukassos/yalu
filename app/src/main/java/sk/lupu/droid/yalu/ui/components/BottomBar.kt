package sk.lupu.droid.yalu.ui.components

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import sk.lupu.droid.yalu.ui.nav.BottomRouteHolder
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun BottomBar(
    navController: NavHostController,
    bottomRouteHolders: List<BottomRouteHolder>
) {
    val navBackStackEntry = navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry.value?.destination
    BottomNavigation {
        bottomRouteHolders.forEach { routeHolder ->
            AddItem(
                routeHolder = routeHolder,
                currentDestination = currentDestination,
                navController = navController
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ScreenPreview() {
    YALUTheme {
        BottomBar(
            navController = rememberNavController(),
            bottomRouteHolders = listOf(
                BottomRouteHolder.History,
                BottomRouteHolder.Map,
                BottomRouteHolder.Settings,
            )
        )
    }
}

@Composable
fun RowScope.AddItem(
    routeHolder: BottomRouteHolder,
    currentDestination: NavDestination?,
    navController: NavHostController
) {
    BottomNavigationItem(
        label = { Text(text = routeHolder.title) },
        icon = {
            Icon(
                imageVector = routeHolder.icon,
                contentDescription = "${routeHolder.title} navigation icon"
            )
        },
        selected = currentDestination?.hierarchy?.any { navDest ->
            navDest.route == routeHolder.route
        } == true,
        onClick = {
            navController.navigate(routeHolder.route)

        },
        unselectedContentColor = MaterialTheme.colors.secondary,
        selectedContentColor = MaterialTheme.colors.secondaryVariant
    )
}