package sk.lupu.droid.yalu.ui.nav

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import sk.lupu.droid.yalu.ui.screens.HistoryScreen
import sk.lupu.droid.yalu.ui.screens.MapScreen
import sk.lupu.droid.yalu.ui.screens.SettingsScreen

@Composable
fun BottomNavGraph(
    navController: NavHostController,
    startDestination: String = BottomRouteHolder.Map.route
) {
    NavHost(
        navController = navController,
        startDestination = startDestination
    ){
        composable(route = BottomRouteHolder.History.route){
            HistoryScreen()
        }
        composable(route = BottomRouteHolder.Map.route){
            MapScreen()
        }
        composable(route = BottomRouteHolder.Settings.route){
            SettingsScreen()
        }
    }
}
