package sk.lupu.droid.yalu.ui.components.map

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import sk.lupu.droid.yalu.ui.theme.YALUTheme

@Composable
fun MapButton(text: String, onClick: () -> Unit) {
    Button(
        modifier = Modifier
            .padding(2.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = MaterialTheme.colors.onPrimary,
            contentColor = MaterialTheme.colors.primary
        ),
        onClick = onClick
    ) {
        Text(text = text, style = MaterialTheme.typography.h6)
    }
}

@Preview
@Composable
fun MapButtonPreview() {
    YALUTheme {
        MapButton(
            text = "+"
        ) {
            /* No op */
        }
        MapButton(
            text = "-"
        ) {
            /* No op */
        }
    }
}
