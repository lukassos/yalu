package sk.lupu.droid.yalu.data.datastore

import androidx.compose.ui.graphics.Color
import androidx.datastore.preferences.core.floatPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.google.maps.android.compose.MapType
import sk.lupu.droid.yalu.ui.theme.*

sealed class SealedPrefs(
    val name: String
){
    object Settings : SealedPrefs(
        name =  "settings"
    ){
        val TRACKING_INTERVAL = stringPreferencesKey("tracking_interval")
        val MAP_TYPE =  stringPreferencesKey("map_type")
        val MAP_MARKER_INTERVAL = stringPreferencesKey("map_marker_interval")
        val MAP_MARKER_COLOR = stringPreferencesKey("map_marker_color")
        val MAP_LINE_COLOR = stringPreferencesKey("map_line_color")
        val MAP_LINE_WIDTH = floatPreferencesKey("map_line_width")

        enum class IntervalEnum (
            val milliseconds:Long,
            val label: String
            ) {
            SEC_1( milliseconds = 1_000,  label = "1 sec"),
            SEC_5( milliseconds = 5_000,  label = "5 sec"),
            SEC_10( milliseconds = 10_000,  label = "10 sec"),
            SEC_30( milliseconds = 30_000,  label = "30 sec"),
            MIN_1( milliseconds = 60_000,  label = "1 minute"),
            MIN_5( milliseconds = 300_000,  label = "5 minute"),
            MIN_10( milliseconds = 600_000,  label = "10 minute"),
            MIN_15( milliseconds = 900_000,  label = "15 minute"),
            MIN_30( milliseconds = 1_800_000,  label = "30 minute"),
        }
        val DEFAULT_INTERVAL =  IntervalEnum.SEC_10
        fun IntervalEnum.toDropdownChoice() = run { this.name to this.label }
        fun MapType.toDropdownChoice() = run { this.name to this.name }

        @Suppress("unused")
        enum class ColorEnum (
            val color:Color,
            val label: String
        ) {
            BROWN( color = brownColor,  label = "Brown"),
            GREEN( color = greenColor,  label = "Green"),
            RED( color = redColor,  label = "Red"),
            BLACK( color = blackColor,  label = "Black"),
            WHITE( color = whiteColor,  label = "White"),

        }
        val DEFAULT_MARKER_COLOR =  ColorEnum.GREEN
        val DEFAULT_LINE_COLOR = ColorEnum.BROWN
        fun ColorEnum.toDropdownChoice() = run { this.name to this.label }
        const val DEFAULT_LINE_WIDTH = 9.0f
    }
}
